var Home = function () {
    return {
        //main function to initiate the module
        init: function () {
            var form_values = new Object();
            var closebutton = '<button class="close" data-dismiss="alert"></button>';

            $('#login_form #save').click(login);
            $('#create_user_form #save').click(createUser);
            $('#resend_activation_form #save').click(resendActivation);
            $('#forgot_password_form #save').click(forgotPassword);
            $('#reset_password_form #save').click(resetPassword);

            var errorlogin = $('#login_form .forminvalid');
            var erroruser = $('#create_user_form .forminvalid');
            var erroractive = $('#resend_activation_form .forminvalid');
            var errorpass = $('#forgot_password_form .forminvalid');

            // Validation of login form
            $('#login_form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    errorlogin.show();
                    Metronic.scrollTo(errorlogin, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    errorlogin.hide();
                }
            });

            // Validation of create user form
            $('#create_user_form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                    firstname: {
                        required: true,
                        minlength:1,
                        maxlength:20,
                    },
                    lastname: {
                        required: true,
                        minlength:1,
                        maxlength:20,
                    },
                    password: {
                        required: true,
                        minlength:5,
                        maxlength:20,
                    },
                    cpassword: {
                        required: true,
                        equalTo:password,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    erroruser.show();
                    Metronic.scrollTo(erroruser, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    erroruser.hide();
                }
            });

            // Validation of send activation form
            $('#resend_activation_form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    erroractive.show();
                    Metronic.scrollTo(erroractive, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    erroractive.hide();
                }
            });

            // Validation of forgot password form
            $('#forgot_password_form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    errorpass.show();
                    Metronic.scrollTo(errorpass, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    errorpass.hide();
                }
            });

            // Validation of create user form
            $('#reset_password_form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    password: {
                        required: true,
                        minlength:5,
                        maxlength:20,
                    },
                    cpassword: {
                        required: true,
                        equalTo:password,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    erroruser.show();
                    Metronic.scrollTo(erroruser, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    erroruser.hide();
                }
            });

            // login
            function login() {
                $('.errcustom').hide();
                $('.success').hide();
                // Validate
                if ($('#login_form').valid() == false)
                    return false;

                // get email password
                form_values['email'] = $('#login_form #email').val();
                form_values['password'] = $('#login_form #password').val();
                Metronic.blockUI({boxed: true});
                // ajax call to save the form values
                $.ajax({
                    url: '/login/alogin',
                    type: "POST",
                    datatype: 'json',
                    data: form_values,
                    success: function(data){
                        var value = JSON.parse(data);
                        if (typeof value.error != 'undefined') {
                            $('#login_form .errcustom').html(closebutton+value.error).show();
                            return;
                        }
                        $('#login_form .success').html(closebutton+'Logged in').show();
                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                });
            }

            // Create user
            function createUser() {
                $('.errcustom').hide();
                $('.success').hide();
                // Validate
                if ($('#create_user_form').valid() == false)
                    return false;

                // get email password
                form_values['email'] = $('#create_user_form #email').val();
                form_values['firstname'] = $('#create_user_form #firstname').val();
                form_values['lastname'] = $('#create_user_form #lastname').val();
                form_values['password'] = $('#create_user_form #password').val();
                form_values['cpassword'] = $('#create_user_form #cpassword').val();
                Metronic.blockUI({boxed: true});
                // ajax call to save the form values
                $.ajax({
                    url: '/login/createAccount',
                    type: "POST",
                    datatype: 'json',
                    data: form_values,
                    success: function(data){
                        var value = JSON.parse(data);
                        if (typeof value.error != 'undefined') {
                            $('#create_user_form .errcustom').html(closebutton+value.error).show();
                            return;
                        }
                        $('#create_user_form .success').html(closebutton+'User created').show();
                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                });
            }

            // resend activation
            function resendActivation() {
                $('.errcustom').hide();
                $('.success').hide();
                // Validate
                if ($('#resend_activation_form').valid() == false)
                    return false;

                // get email password
                form_values['email'] = $('#resend_activation_form #email').val();
                Metronic.blockUI({boxed: true});
                // ajax call to save the form values
                $.ajax({
                    url: '/login/resendActivation',
                    type: "POST",
                    datatype: 'json',
                    data: form_values,
                    success: function(data){
                        var value = JSON.parse(data);
                        if (typeof value.error != 'undefined') {
                            $('#resend_activation_form .errcustom').html(closebutton+value.error).show();
                            return;
                        }
                        $('#resend_activation_form .success').html(closebutton+'Mail sent').show();
                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                });
            }

            // forgot password
            function forgotPassword() {
                $('.errcustom').hide();
                $('.success').hide();
                // Validate
                if ($('#forgot_password_form').valid() == false)
                    return false;

                // get email password
                form_values['email'] = $('#forgot_password_form #email').val();
                Metronic.blockUI({boxed: true});
                // ajax call to save the form values
                $.ajax({
                    url: '/login/forgotPassword',
                    type: "POST",
                    datatype: 'json',
                    data: form_values,
                    success: function(data){
                        var value = JSON.parse(data);
                        if (typeof value.error != 'undefined') {
                            $('#forgot_password_form .errcustom').html(closebutton+value.error).show();
                            return;
                        }
                        $('#forgot_password_form .success').html(closebutton+'Mail sent').show();
                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                });
            }

            // Reset password
            function resetPassword() {
                $('.errcustom').hide();
                $('.success').hide();
                // Validate
                if ($('#reset_password_form').valid() == false)
                    return false;

                // get password
                form_values['forgot_password_key'] = $('#reset_password_form #forgot_password_key').val();
                form_values['password'] = $('#reset_password_form #password').val();
                form_values['cpassword'] = $('#reset_password_form #cpassword').val();
                Metronic.blockUI({boxed: true});
                // ajax call to save the form values
                $.ajax({
                    url: '/login/updatePassword',
                    type: "POST",
                    datatype: 'json',
                    data: form_values,
                    success: function(data){
                        var value = JSON.parse(data);
                        if (typeof value.error != 'undefined') {
                            $('#reset_password_form .errcustom').html(closebutton+value.error).show();
                            return;
                        }
                        $('#reset_password_form .success').html(closebutton+'Password reset').show();
                        $('#reset_password_form #save').hide();
                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                });
            }
        }
    };
}();