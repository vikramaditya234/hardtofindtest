/**
Core script to handle the entire theme and core functions
**/
var Userfilter = function () {
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true,
                dateFormat: 'dd-M-yy'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */
    }

    // Save filter in database
    var savefilter = function() {
        // Get all the values
        $('#loader_anim').show();
        var dataO = new Object();
        dataO.filter_name = $('#filter_name').val();
        dataO.app_source = $('#app_source').val();
        //dataO.percentage_user = $('#percentage_user').val();
        dataO.country = $("#country_list").select2("val");
        dataO.app_source = $('#app_source').val();
        dataO.is_anonymous = $('.anonymous:checked').val();
        dataO.app_install_start_date = $("#app_install_start_date").datepicker("getFormattedDate");
        dataO.app_install_end_date = $("#app_install_end_date").datepicker("getFormattedDate");
        // Check if this is a update or save new filter
        var filter_id = $('#user_filter_id').val();
        if ((filter_id == 0) || (filter_id.length == 0)) {
            // Save new test
            dataO.filter_id = 0;
            var request_url = user_filter_create_url;
        }
        else {
            // Update existing test
            dataO.filter_id = filter_id;
            var request_url = user_filter_update_url+'/'+filter_id;
        }
        $.ajax({
            url: request_url,
            type: "POST",
            data: dataO,
            datatype: 'json',
            success: function(data){
                if (data.length == 0) {
                    // Show error
                    $('#errormessage')  .html('Error in saving filter')
                                        .removeClass('hide');
                    $('#loader_anim').hide();
                    return;
                }
                value = JSON.parse(data);
                // Check if there are any errors
                if (typeof value.error != 'undefined') {
                    // Show error
                    $('#errormessage')  .html(value.error)
                                        .removeClass('hide');
                    $('#loader_anim').hide();
                    return;
                }
                // Refresh the page
                location.reload();
            },
            complete: function() {
                $('#loader_anim').hide();
            }
        });
    }

    // Open modal with filter values
    var openfilter = function(thisobj) {
        // Get values from the table
        var row = thisobj.parents('tr');
        var filter_id = row.find('.user_filter_id').html();
        row = $('#all_filter_values tr#'+filter_id);
        $('#newfilter #user_filter_id').val(filter_id);
        $('#filter_name').val(row.find('.filter_name').html())
        $('#app_source').val(row.find('.app_source').html());
        //$('#percentage_user').val(row.find('.percentage_user').html());
        $.uniform.update($(".anonymous").attr('checked', false));
        $.uniform.update($('.anonymous[value='+row.find('.anonymous').html()+']').attr("checked",true));
        $("#app_install_start_date").datepicker("setDate", row.find('.app_install_start_date').html());
        $("#app_install_end_date").datepicker("setDate", row.find('.app_install_end_date').html());

        var country_list = row.find('.country_list').html();
        $("#country_list").select2('val', country_list.split(','));

        $('#newfilter .modal-title').html('Edit User Filter');
        $('#newfilter').modal('show');
    }

    // Open modal for new filter
    var newfilter = function() {
        $('#newfilter #user_filter_id').val(0);
        $('#filter_name').val(' ');
        $('#app_source').val(' ');
        //$('#percentage_user').val(0);
        $("#country_list").select2('val', []);
        $.uniform.update($(".anonymous").attr('checked', false));
        $.uniform.update($(".anonymous[value='b']").attr('checked', true));
        $("#app_install_start_date").datepicker("setDate", null);
        $("#app_install_end_date").datepicker("setDate", null);

        $('#newfilter .modal-title').html('New User Filter');
        $('#newfilter').modal('show');
    }

    return {

        init: function () {
            handleDatePickers();
            $('#country_list').select2();
            $('#savefilter').click(savefilter);
            $('#newfilter_but').click(newfilter);
            $('.openfilter').click(function (e) {e.preventDefault(); openfilter($(this))});
            var form = $('#newfilter form');
            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    //account
                    filter_name: {
                        minlength: 5,
                        required: true
                    },
                    /*
                    percentage_user: {
                        number: true,
                        min: 0,
                        max: 50,
                    },
                    */
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    $('#form-validation').show();
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    $('#form-validation').hide();
                }
            });
        }
    };

}();