/**
Script to fetch information from twitter
**/
var Start = function () {
    // Get user information from twitter
    var getUserInformation = function() {
        if ($('#twitter_form form').valid() == false) {
            return;
        }
        $('#form-validation').hide();
        // Get all the values
        $('#loader_anim').show();
        var dataO = new Object();
        dataO.twitter_username = $('#twitter_username').val();
        $.ajax({
            url: get_user_information_url,
            type: "POST",
            data: dataO,
            datatype: 'json',
            success: function(data){
                if (data.length == 0) {
                    // Show error
                    $('#errormessage')  .html('Error in getting information from twitter')
                                        .removeClass('hide');
                    $('#loader_anim').hide();
                    $('#chart_div').hide();
                    return;
                }
                value = JSON.parse(data);
                // Check if there are any errors
                if (typeof value.error != 'undefined') {
                    // Show error
                    $('#errormessage')  .html(value.error)
                                        .removeClass('hide');
                    $('#loader_anim').hide();
                    $('#chart_div').hide();
                    return;
                }
                $('#chart_div').show();
                $('#errormessage').addClass('hide');

                // Convert object to array; this should be unnecessary there is a direct way
                var tweets_by_hours = [];
                tweets_by_hours.push(['Hour', 'Tweets']);
                $.each(value.tweets_by_hours, function(key, val) {
                    tweets_by_hours.push([key, val]);
                });
                // Got hours, draw histogram now
                var data = google.visualization.arrayToDataTable(tweets_by_hours);
                var options = {
                  title: 'Tweets by hours of the day',
                  hAxis: {
                        title: 'Hours',
                  },
                  yAxis: {
                        title: 'Number of tweets',
                  },
                  legend: { position: 'none' },
                };
                var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            },
            complete: function() {
                $('#loader_anim').hide();
            }
        });
    }

    return {

        init: function () {
            $('#get_information').click(getUserInformation);
            google.load("visualization", "1", {packages:["corechart"]});
            $('#twitter_form form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    twitter_username: {
                        required: true,
                        maxlength: 255,
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    $('#form-validation').show();
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
            });
        }
    };

}();