var Login = function () {

	var handleLogin = function() {

		$('#login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
            },

            messages: {
                username: {
                    required: "Username is required."
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
        });

		$('#login-form .submit').click(function (e) {e.preventDefault(); login();});

        $('#login-form input').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault(); 
                if ($('.login-form').validate().form()) {
                    login(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

        // login
        function login() {
            $('.errcustom').hide();
            $('.success').hide();

            // get email password
            var form_values = new Object();
            form_values['email'] = $('#login-form #email').val();
            form_values['password'] = $('#login-form #password').val();
            Metronic.blockUI({boxed: true});
            // ajax call to save the form values
            $.ajax({
                url: '/apps.tools/login/alogin',
                type: "POST",
                datatype: 'json',
                data: form_values,
                success: function(data){
                    var value = JSON.parse(data);
                    if (typeof value.error != 'undefined') {
                        $('#login-form .errcustom').html(value.error).show();
                        return;
                    }
                    location.reload();
                },
                complete: function () {
                    Metronic.unblockUI();
                }
            });
        }
	}

	var handleForgetPassword = function () {
		$('#forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },

            messages: {
                email: {
                    required: "Email is required."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                forgotPassword();
            }
        });

        $('#forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    forgotPassword();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function () {
            jQuery('#login-form').hide();
            jQuery('#forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('#login-form').show();
            jQuery('#forget-form').hide();
        });

        // forgot password
        function forgotPassword() {
            $('.errcustom').hide();
            $('.success').hide();
            // Validate
            if ($('#forget-form').valid() == false)
                return false;

            // get email password
            var email = $('#forget-form #email').val();
            Metronic.blockUI({boxed: true});
            // ajax call to save the form values
            $.ajax({
                url: '/apps.tools/login/forgotPassword',
                type: "POST",
                datatype: 'json',
                data: {'email': email},
                success: function(data){
                    var value = JSON.parse(data);
                    if (typeof value.error != 'undefined') {
                        $('#forget-form .errcustom').html(value.error).show();
                        return;
                    }
                    $('#forget-form .success').html('Mail sent').show();
                },
                complete: function () {
                    Metronic.unblockUI();
                }
            });
        }
	}

	var handleRegister = function () {

         $('#register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                fullname: {
	                    required: true
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
	                password: {
	                    required: true
	                },
	                cpassword: {
	                    equalTo: "#register_password"
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                createUser();
	            }
	        });

			$('#register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('#register-form').validate().form()) {
	                    createUser();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('#login-form').hide();
	            jQuery('#register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('#login-form').show();
	            jQuery('#register-form').hide();
	        });

            // Create user
            function createUser() {
                $('.errcustom').hide();
                $('.success').hide();

                // get email password
                var form_values = Object();
                form_values['email'] = $('#register-form #email').val();
                form_values['name'] = $('#register-form #name').val();
                form_values['password'] = $('#register-form #password').val();
                form_values['cpassword'] = $('#register-form #cpassword').val();
                Metronic.blockUI({boxed: true});
                // ajax call to save the form values
                $.ajax({
                    url: '/apps.tools/login/createAccount',
                    type: "POST",
                    datatype: 'json',
                    data: form_values,
                    success: function(data){
                        var value = JSON.parse(data);
                        if (typeof value.error != 'undefined') {
                            $('#register-form .errcustom').html(value.error).show();
                            return;
                        }
                        $('#register-form .success').html('User created you will receive activation mail').show();
                        $('#register-form #email').val('');
                        $('#register-form #name').val('');
                        $('#register-form #password').val('');
                        $('#register-form #cpassword').val('');
			            jQuery('#login-form').show();
			            jQuery('#register-form').hide();
                    },
                    complete: function () {
                        Metronic.unblockUI();
                    }
                });
            }

	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            handleRegister();        
	       
        }

    };

}();