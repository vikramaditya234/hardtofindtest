<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Start extends MY_Controller {

    public function __construct() {
        parent::__construct();
        array_push($this->javascript, JS_PATH.'start.js');
    }

    // Show form to get twitter username from user
    public function index() {
        // Load JS for start page
        $this->_render('pages/start');
    }

    // Get username information from twitter
    public function getUserInformation() {
        $form_validate_result = $this->formValidate();
        if ($form_validate_result != true) {
            $this->view_args['error'] = $form_validate_result;
            $this->_render('error', 'JSON');
            return;
        }

        // Send username to twitter to get user last 500 feeds
        $twitter_username = $this->input->post('twitter_username');

        $tweets_by_hours = $this->getUserTweetDates($twitter_username);
        if ($tweets_by_hours == false) {
            $this->view_args['error'] = $this->lang->line('unable_to_get_tweets');;
            $this->_render('error', 'JSON');
            return;
        }
        // Send back user feeds and number of feeds by hour
        $this->view_args['tweets_by_hours'] = $tweets_by_hours;
        $this->_render('success', 'JSON');
    }

    // Get user MAX_TWEETS_FETCHED tweet dates
    private function getUserTweetDates($username) {
        // Max id is the last tweet id, need to fetch the next batch of tweets
        $max_id = '';
        // Total tweet processed
        $tweet_count = 0;
        $total_tweet_dates = array();
        // Loop till dates of MAX_TWEETS_FETCHED are fethed or if tweets limit is hit 
        while ($tweet_count < MAX_TWEETS_FETCHED) {
            $parameters = array(
                                'q' => 'from:'.$username,
                                'count' => MAX_TWEETS_FETCHED,
                                );
            if (!empty($max_id)) {
                $parameters['max_id'] = $max_id;
            }
            $response = $this->twitter_connection->get('search/tweets', $parameters);
            if (!isset($response->statuses) || empty($response->statuses)) {
                // Error in response
                log_message('error', __FILE__.':'.__LINE__.' Error in response from twitter, no statuses for: '.print_r($parameters, true));
                return false;
            }

            // Loop through the tweets to get time of tweet
            foreach ($response->statuses as $key => $tweet) {
                $tweet_count++;
                // Check if desired number of tweets are fetched
                if ($tweet_count > MAX_TWEETS_FETCHED) {
                    break;
                }
                // Get date of the tweet and convert it to hours
                if (!isset($tweet->created_at) || empty($tweet->created_at) || 
                    !isset($tweet->id) || empty($tweet->id)) {
                    log_message('debug', __FILE__.':'.__LINE__.' Tweets missing "created at" or "id" field at: '.$key.' for: '.print_r($parameters, true));
                    continue;
                }
                // Get tweet time in format so to get hours only
                $tweet_hour = date(TWEET_TIME, strtotime($tweet->created_at));
                if (!isset($total_tweet_dates[$tweet_hour])) {
                    $total_tweet_dates[$tweet_hour] = 0;
                }
                $total_tweet_dates[$tweet_hour]++;
                $max_id = $tweet->id;
            }

            if (!isset($response->search_metadata) || empty($response->search_metadata)) {
                // Error in response
                log_message('error', __FILE__.':'.__LINE__.' Error in response from twitter, no search_metadata for: '.print_r($parameters, true));
                return false;
            }

            // Check if there are more results
            if (!isset($response->search_metadata->next_results) || empty($response->search_metadata->next_results)) {
                log_message('error',' doing break');
                break;
            }
        }

        return $total_tweet_dates;
    }

    // Validate user form values
    private function formValidate() {
        $twitter_username = $this->input->post('twitter_username');
        // Validate if username is provided in the request
        if ($twitter_username == false) {
            // No username provided send back error
            return $this->lang->line('no_twitter_username');
        }

        // Validate is username is of max MAX_USERNAME_LENGTH character
        if (strlen($twitter_username) > MAX_USERNAME_LENGTH) {
            // username more than MAX_USERNAME_LENGTH
            return $this->lang->line('twitter_username_too_long');
        }

        return true;
    }
}