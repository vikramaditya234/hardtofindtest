<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'third_party/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;

class MY_Controller extends CI_Controller{
    
    protected $view_args = array();
    protected $data = array();
    protected $pageName = FALSE;
    protected $template = "main";
    protected $javascript = array();
    protected $onloadscript = array();
    protected $css = array();
    protected $fonts = array();
    //Page Meta
    protected $page_title = FALSE;
    protected $showNav = TRUE;
    protected $showSidebar = TRUE;
    protected $sidebar_select = NULL;
    protected $twitter_connection = NULL;

    function __construct() {   
        session_start();
        parent::__construct();
        $this->page_title = 'Home';
        $this->loadJS();
        $this->loadScript();
        $this->loadCSS();
        $this->data['user'] = array();
        $this->lang->load("common","english");

        $this->twitter_connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSRUMER_SECRET);
    }

    // Load common JS
    private function loadJS() {
        $javascript = array(
                        base_url(PLUGINS_PATH.'jquery-1.11.0.min.js'),
                        base_url(PLUGINS_PATH.'jquery-migrate-1.2.1.min.js'),
                        base_url(PLUGINS_PATH.'jquery-ui/jquery-ui-1.10.3.custom.min.js'),
                        base_url(PLUGINS_PATH.'bootstrap/js/bootstrap.min.js'),
                        base_url(PLUGINS_PATH.'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'),
                        base_url(PLUGINS_PATH.'jquery-slimscroll/jquery.slimscroll.min.js'),
                        base_url(PLUGINS_PATH.'jquery.blockui.min.js'),
                        base_url(PLUGINS_PATH.'uniform/jquery.uniform.min.js'),
                        base_url(PLUGINS_PATH.'bootstrap-switch/js/bootstrap-switch.min.js'),
                        base_url(PLUGINS_PATH.'jquery-validation/js/jquery.validate.min.js'),
                        base_url(PLUGINS_PATH.'jquery-validation/js/additional-methods.min.js'),
                        base_url(PLUGINS_PATH.'select2/select2.min.js'),
                        base_url(PLUGINS_PATH.'bootstrap-datepicker/js/bootstrap-datepicker.js'),
                        'https://www.google.com/jsapi',
                        base_url(JS_PATH.'metronic.js'),
                        base_url(JS_PATH.'quick-sidebar.js'),
                        );
        $this->javascript = $javascript;
    }

    // Load common onloadscript
    private function loadScript() {
        $onloadscript = array(
                            'get_user_information_url = "'.site_url(USER_INFORMATION_URL).'"',
                            'Metronic.init()', // init metronic core componets
                            'Metronic.setAssetsPath("'.site_url(IMAGE_PATH).'/")', // Image resource path
                            'Metronic.setGlobalImgPath("")', // Image resource path
                            'QuickSidebar.init()', // init quick sidebar
                            'Start.init()', // init quick sidebar
                            );
        $this->onloadscript = $onloadscript;
    }

    // Load common CSS
    private function loadCSS() {
        $css = array(
                PLUGINS_PATH.'font-awesome/css/font-awesome.min.css',
                PLUGINS_PATH.'simple-line-icons/simple-line-icons.min.css',
                PLUGINS_PATH.'bootstrap/css/bootstrap.min.css',
                PLUGINS_PATH.'uniform/css/uniform.default.css',
                PLUGINS_PATH.'bootstrap-switch/css/bootstrap-switch.min.css',
                PLUGINS_PATH.'bootstrap-daterangepicker/daterangepicker-bs3.css',
                PLUGINS_PATH.'select2/select2.css',
                PLUGINS_PATH.'select2/select2-bootstrap.css',
                PLUGINS_PATH.'bootstrap-datepicker/css/datepicker3.css',
                CSS_PATH.'components.css',
                CSS_PATH.'plugins.css',
                CSS_PATH.'layout.css',
                CSS_PATH.'themes/default.css',
                CSS_PATH.'custom.css',
                );
        $this->css = $css;
    }

    // Render the web view
    protected function _render($view, $renderData="FULLPAGE") {
        switch ($renderData) {
            case "JSON"     :
                echo json_encode($this->view_args);
            return;
            case "PAGE"     :
                $this->data = array_merge($this->data, $this->view_args);
                return $this->load->view($view, $this->data, TRUE);
            break;
            case "FULLPAGE" :
            default         : 
                $this->data = array_merge($this->data, $this->view_args);
                //static
                $toTpl["javascript"] = $this->javascript;
                $toTpl["onloadscript"] = $this->onloadscript;
                $toTpl["css"] = $this->css;
                $toTpl["fonts"] = $this->fonts;
                
                //meta
                $toTpl["page_title"] = $this->page_title;
                $toTpl["sidebar_select"] = $this->sidebar_select;

                //gather parts of the page
                $toBody["content_body"] = $this->load->view($view, array_merge($this->data, $toTpl), TRUE);
                $toBody["footer"] = $this->load->view("template/footer",'',TRUE);
                $nav = '';
                if ($this->showNav = TRUE) {
                    $nav = $this->load->view("template/nav",'',TRUE);
                }
                $toBody["header"] = $this->load->view("template/header", array('nav' => $nav), TRUE);

                $toBody["sidebar"] = '';
                if ($this->showSidebar = TRUE) {
                    $toBody["sidebar"] = $this->load->view("template/sidebar",'',TRUE);
                }

                $toTpl["body"] = $this->load->view("template/".$this->template, $toBody, TRUE);
                
                //render view
                $this->load->view("template/skeleton",$toTpl);
            break;
        }
    }
}
