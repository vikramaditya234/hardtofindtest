<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Generate hash of given string
function my_generate_hash($plainText, $salt = null) {
    if ($salt === null) {
        $salt = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
    }
    else {
        $salt = substr($salt, 0, SALT_LENGTH);
    }
    return $salt . sha1($salt . $plainText);
}
// Generate random key
function my_generate_random_key() {
    return md5(uniqid(mt_rand(), false));
}

function my_sanitize($text) {
    return strtolower(strip_tags(trim(($text))));
}
?>