<?php
if (isset($ajax)) {
    $msg['success'] = 1;
    echo json_encode($msg);
    return;
}
echo $msg;
