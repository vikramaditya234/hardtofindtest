<!-- BEGIN SAMPLE TABLE PORTLET-->
<div id="twitter_form">
    <div class="alert alert-danger hide" id="errormessage"></div>
    <div class="alert alert-danger display-hide" id="form-validation">
        <button class="close" data-close="alert"></button>
        Submission error
    </div>
    <form action="#" class="form-horizontal form-row-sepe">
        <div class="col-md-8">
            <div class="form-group">
                <label class="control-label col-md-3">Twitter username</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="twitter_username" name="twitter_username" placeholder="Twitter username">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <button type="button" id="get_information" class="btn blue">Submit</button>
        </div>

    </form>
    <div class="clearfix"></div>
</div>
<div id="chart_div"></div>
<!-- END SAMPLE TABLE PORTLET-->
