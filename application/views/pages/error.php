<?php
if (isset($ajax)) {
    $msg['error'] = 1;
    echo json_encode($msg);
    return;
}
echo $msg;
