<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $page_title ?></title>
<meta name="viewport" content="width=device-width">

<!-- Add CSS-->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<?php foreach($css as $c):?>
<link rel="stylesheet" href="<?php echo base_url($c);?>" rel="stylesheet" type="text/css">
<?php endforeach;?>

</head>
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
   <?php echo $body ?>
    <!-- extra JS-->
    <?php foreach($javascript as $j):?>
    <script src="<?php echo $j;?>" type="text/javascript"></script>
    <?php endforeach;?>
            
    <!-- On load script-->
    <?php foreach($onloadscript as $ols):?>
        <script>
            <?php echo $ols.';'; ?>
        </script>
    <?php endforeach;?>
</body>
</html>
