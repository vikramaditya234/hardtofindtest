<?php echo $header ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php echo $sidebar;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div id="loader_anim" style="display: none;"><img src="<?php echo site_url(IMAGE_PATH.'loading-spinner-blue.gif'); ?>" /></div>
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            <?php echo $page_title ?>
        </h3>
        <!-- END PAGE HEADER-->

       <?php echo $content_body ?>
    </div>
</div>
<!-- END CONTENT -->
<br>
<?php echo $footer ?>
