<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
 |--------------------------------------------------------------------------
| Custom
|--------------------------------------------------------------------------
|
*/


define('DEBUG', 0);

define('IMAGE_PATH','resources/img/');
define('JS_PATH','resources/js/');
define('CSS_PATH','resources/css/');
define('PLUGINS_PATH','resources/plugins/');
define('UPLOAD','resources/upload/');

//URLs
define('USER_INFORMATION_URL', '/start/getUserInformation');
define('TWITTER_AUTH_CALLBACK_URL', '/start/twitterAuthCallback');

define('MAX_USERNAME_LENGTH', 255);
define('MAX_TWEETS_FETCHED', 500);
define('TWEET_TIME', 'H');

define('TWITTER_CONSUMER_KEY', 'hZYkT7lWAyDDDWyR4UIj2hwvp');
define('TWITTER_CONSRUMER_SECRET', 'FmkOqCUDspNhLZHZ00JXKJirExl1wHa9W7dKKZ2NlhlvY8qwr6');

/* End of file constants.php */
/* Location: ./application/config/constants.php */