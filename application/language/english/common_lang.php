<?php
$lang['no_twitter_username'] = 'No username provided';
$lang['twitter_username_too_long'] = 'Username should be less than 256 characters';
$lang['twitter_auth_error'] = 'Error in getting authorization from twitter';
$lang['unable_to_get_tweets'] = 'Error in getting tweets from twitter';
